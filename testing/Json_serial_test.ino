#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

#define WLAN_SSID       "LV_WIFI" // enter your WiFi SSID
#define WLAN_PASS       "31415926" // this is your WiFi password

String ssid="LV_WIFI";
WiFiClient client;

void setup() {
  Serial.begin(115200);

  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);
  
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());
}

void loop() {
  if(Serial.read() == 'j'){
    DynamicJsonDocument doc(1024);
    int n = WiFi.scanNetworks(false,true);
    for(int i = 0; i < n; ++i)
    {
      if(WiFi.SSID(i)==ssid){
        doc[WiFi.BSSIDstr(i)] = WiFi.RSSI(i);
      }
    }
    serializeJson(doc, Serial);
    Serial.println();
  }
}
