import paho.mqtt.client as mqtt

mqtt_username = "pi"
mqtt_password = "31415926"
mqtt_topic = "esp_test"
mqtt_broker_ip = "192.168.2.188"

client = mqtt.Client()
client.username_pw_set(mqtt_username, mqtt_password)

def on_connect(client, userdata, flags, rc):
    client.subscribe(mqtt_topic)

def on_message(client, userdata, msg):
	topic = msg.topic
	m_decode = str(msg.payload.decode("utf-8", "ignore"))
	print("data Received", m_decode)

client.on_connect = on_connect
client.on_message = on_message
client.connect(mqtt_broker_ip, 1883)
client.loop_forever()
client.disconnect()
