/*
 * ESP8266 (Adafruit HUZZAH) Mosquitto MQTT Publish Example
 * Thomas Varnish (https://github.com/tvarnish), (https://www.instructables.com/member/Tango172)
 * Made as part of my MQTT Instructable - "How to use MQTT with the Raspberry Pi and ESP8266"
 */
#include <ArduinoJson.h>
#include <ESP8266WiFi.h> // Enables the ESP8266 to connect to the local network (via WiFi)
#include <PubSubClient.h> // Allows us to connect to, and publish to the MQTT broker

const char* ssid = "LV_WIFI";
const char* wifi_password = "31415926";

const char* mqtt_server = "192.168.2.188";
const char* mqtt_topic_data = "esp_data";
const char* mqtt_topic_edit_point = "esp_edit_point";
const char* mqtt_username = "pi";
const char* mqtt_password = "31415926";
// The client id identifies the ESP8266 device. Think of it a bit like a hostname (Or just a name, like Greg).
const char* clientID = "1";

WiFiClient wifiClient;
PubSubClient client(mqtt_server, 1883, wifiClient); // 1883 is the listener port for the Broker

char buffer[512];

void setup() {
  Serial.begin(115200);

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  if (client.connect(clientID, mqtt_username, mqtt_password)) {
    Serial.println("Connected to MQTT Broker!");
  }
  else {
    Serial.println("Connection to MQTT Broker failed...");
  }
  
}

void loop() {
  char serial = Serial.read();
  if(serial == 'e'){
      MQTT(mqtt_topic_edit_point);
    }else if(serial == 'd'){
      MQTT(mqtt_topic_data);
    }
}


void MQTT(const char* mqtt_topic){
      DynamicJsonDocument edit_point(1024);
      int n = WiFi.scanNetworks(false,true);
      for(int i = 0; i < n; ++i)
      {
        if(WiFi.SSID(i)==ssid){
          edit_point[WiFi.BSSIDstr(i)] = WiFi.RSSI(i);
        }
      }
      serializeJson(edit_point, Serial);
      serializeJson(edit_point, buffer);
      Serial.println();

      if (client.publish(mqtt_topic, buffer)) {
        Serial.print("Data send : ");
        Serial.println(mqtt_topic);
      } else {
        Serial.print("Data failed to send. Reconnecting to MQTT Broker and trying again : ");
        Serial.println(mqtt_topic);
        client.connect(clientID, mqtt_username, mqtt_password);
        delay(10);
        client.publish(mqtt_topic, buffer);
      }
}
