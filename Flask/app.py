from flask import Flask, render_template, url_for, redirect
from forms import EditForm

espeditpoint = True
posts = [100]

app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'


@app.route('/', methods=['GET', 'POST'])
def edit_point():
	global espeditpoint
	if(espeditpoint):
		form = EditForm()
		if form.validate_on_submit():
			print(form.data)
			espeditpoint = False
		return render_template('edit_point.html', form=form)
	else:
		return redirect(url_for('show_data'))


@app.route('/data', methods=['GET'])
def show_data():
	return render_template('show_data.html', posts=posts)


@app.errorhandler(404)
def page_not_found(e):
	return render_template('notFound.html'), 404


if __name__ == '__main__':
	app.run(debug=True)
