from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class EditForm(FlaskForm):
    sector = StringField('Sector', validators=[DataRequired()])
    submit = SubmitField('Send')
